#include <iostream>
#include <windows.h>
using namespace std;

void input_data();

float luas_lingkaran(float jari);

float keliling_lingkaran();

float volume_tabung(float luas, float tinggi);

int main(){
	char ulangi;
	
	do
	{
		input_data();
		
		cout << "Ulangi program? (y/n) "; cin >> ulangi;
		cout << endl;
		
	} while (ulangi == 'y' || ulangi == 'Y');
	
	cout << "Program selesai, Terima kasih :)\n";
	
	cout << endl;
	system("pause");
	return 0;
}

void input_data(){
	int pilih;
	float luas = 0, keliling = 0, volume = 0;
	float tinggi, jariJari;

	cout << "Hitung Lingkaran ==========\n";
	cout << "1. Luas Lingkaran\n2. Keliling Lingkaran\n3. Volume Tabung\nPilih: ";
	cin >> pilih;
	
	switch(pilih){
		case 1:
			cout << "Jari - jari	: "; cin >> jariJari;
			luas = luas_lingkaran(jariJari);
			cout << "Luas Lingkaran adalah " << luas << endl;
		break;
		
		case 2:
			keliling = keliling_lingkaran();
			cout << "Keliling Lingkaran adalah " << keliling << endl;
		break;
		
		case 3:
			cout << "Jari - jari	: "; cin >> jariJari;
			cout << "Tinggi		: "; cin >> tinggi;
			volume = volume_tabung(jariJari, tinggi);
			cout << "Volume Tabung adalah " << volume << endl;
		break;
		
		default: cout << "Input Salah :)\n";
	}
}

float luas_lingkaran(float jari){
	return jari * jari * 3.14;
}

float keliling_lingkaran(){
	float jari;
	cout << "Jari - jari	: "; cin >> jari;
	return 2 * 3.14 * jari;
}

float volume_tabung(float jari, float tinggi) {
	float luas = 3.14 * jari * jari;
	return luas * tinggi;
}
