#include <iostream>
#include <windows.h>
using namespace std;

int main(){
	int ayah, ibu, anak1, anak2;
	int biaya = 0;
	
	cout << "Umur Ayah\t: "; cin >> ayah;
	cout << "Umur Ibu\t: "; cin >> ibu;
	cout << "Umur Anak 1\t: "; cin >> anak1;
	cout << "Umur Anak 2\t: "; cin >> anak2;
	
	// Dewasa 21 ke atas dapet 30rb
	// Remaja 13 - 20 dapet 20 rb
	// Anak-anak 12 ke bawah dapet 15 rb
	
	if (ayah > 20){
		biaya += 30000;
	}
	else if(ayah > 12){
		biaya += 20000;
	}
	else {
		biaya += 15000;
	}
	
	
	if (ibu > 20){
		biaya += 30000;
	}
	else if(ibu > 12){
		biaya += 20000;
	}
	else {
		biaya += 15000;
	}
	
	
	if (anak1 > 20){
		biaya += 30000;
	}
	else if(anak1 > 12){
		biaya += 20000;
	}
	else {
		biaya += 15000;
	}
	
	
	if (anak2 > 20){
		biaya += 30000;
	}
	else if(anak2 > 12){
		biaya += 20000;
	}
	else {
		biaya += 15000;
	}
	
	biaya *= 30;
	
	cout << "Tagihan satu bulan adalah Rp." << biaya << endl;
	
	system("pause");
	return 0;
}
