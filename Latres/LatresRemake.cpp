#include <iostream>
#include <iomanip>
#include <windows.h>
using namespace std;

string nama, dosen, namk[5];
char nim[9], kmk[5][5], nmkh[5];
int jmk, nmk[5], sks[5], totalsks=0, totalnilai=0, k[5];

void nilai(int n, int i){
	if (n>100){
		nmkh[i] = 'E'; k[i]=0*sks[i]; totalnilai+=k[i]; }
	else if (n>80 && n<=100){
		nmkh[i] = 'A'; k[i]=4*sks[i]; totalnilai+=k[i]; }
	else if (n>60){
		nmkh[i] = 'B'; k[i]=3*sks[i]; totalnilai+=k[i]; }
	else if (n>40){
		nmkh[i] = 'C'; k[i]=2*sks[i]; totalnilai+=k[i]; }
	else if (n>20){
		nmkh[i] = 'D'; k[i]=1*sks[i]; totalnilai+=k[i]; }
	else{
		nmkh[i] = 'E'; k[i]=0*sks[i]; totalnilai+=k[i]; }
}

void input(){
	cout<<"Input Data KRS ============\n";
	cout<<"Nama\t\t\t: "; getline(cin,nama);
	cout<<"NIM\t\t\t: "; cin>>nim;
	cout<<"Dosen Wali\t\t: "; cin.ignore(); getline(cin,dosen);
	cout<<"Jumlah Mata Kuliah\t: "; cin>>jmk;
	cout<<endl;
	
	for (int i = 0; i < jmk; i++)
	{
		cout<<"   Matkul ke-"<<i+1<<endl;
		cout<<"   Kode Matkul\t\t: "; cin>>kmk[i];
		cout<<"   Nama Matkul\t\t: "; cin>>namk[i];
		cout<<"   SKS\t\t\t: "; cin>>sks[i];
		cout<<"   Nilai Akhir Matkul\t: "; cin>>nmk[i];
		cout<<endl;
		
		totalsks+=sks[i];
		nilai(nmk[i], i);
	}
}

void output(){
	if (nim[0]=='1')
	{
		cout << "\nUPN \"Veteran\" Yogyakarta";
		if (nim[1]=='2'){
			cout << "\nFakultas Teknologi Industri";
			if (nim[2]=='1')
				cout << "\nTeknik Kimia D3";
			else if (nim[2]=='2')
				cout << "\nTeknik Industri";
			else if (nim[2]=='3')
			  cout << "\nTeknik Informatika";
			else if (nim[2]=='4')
			  cout << "\nSistem Informasi";
		}
		else {
			cout<<"\nFakultas Lain";
		}
		cout <<"\nNIM\t\t: "<<nim<<endl;
		cout<<"Nama\t\t: "<<nama<<endl;
		cout<<"Angkatan\t: 20"<<nim[3]<<nim[4]<<endl<<endl;
		cout<<"\t\t\tKARTU HASIL STUDI\t\t\t"<<endl;
		cout<<"--------------------------------------------------------------------------------"<<endl;
		cout<<"No | Kode MK |      MataKuliah      | SKS | Nilai Angka | Nilai Huruf | Total"<<endl;
		cout<<"--------------------------------------------------------------------------------"<<endl;
		for (int i = 0; i < jmk; i++)
		{
			cout<<setw(2)<<i+1<<setw(10)<<kmk[i]<<setw(23)<<namk[i]<<setw(5)<<sks[i]<<setw(15)<<nmk[i]<<setw(14)<<nmkh[i]<<setw(8)<<k[i];
			cout<<endl;
		}
		cout<<"--------------------------------------------------------------------------------"<<endl;
		cout<<"Total SKS\t: "<<totalsks<<endl;
		cout<<"Total Nilai\t: "<<totalnilai<<endl;
		cout<<"IP = "<<totalnilai*1.0/totalsks<<endl;
		cout<<"--------------------------------------------------------------------------------"<<endl;
		cout<<"Mengetahui,\nDosen Wali\n\n\n"<<dosen;
	}
	else {
		cout<<endl<<nama<<" bukan mahasiswa UPN \"Veteran\" Yogyakarta\n";
	}
}

main(){
	input();
	output();
	cout<<endl<<endl;
	system("pause");
}
