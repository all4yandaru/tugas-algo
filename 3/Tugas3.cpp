#include <iostream>
#include <windows.h>
using namespace std;

int main(){
	string username, password;
	int pilih;
	float harian, uts, uas, nilai_akhir;
	
	cout << "Login Akun ==========\n";
	cout << "Username\t: "; cin >> username;
	cout << "Password\t: "; cin >> password;
	
	cout << "=====================\n";
	if (username == "admin" && password == "123"){
		cout << "Login berhasil!\n\n";
		
		cout << "Input Nilai Mata Kuliah\n";
		cout << "1. Algoritma dan Pemrograman\n";
		cout << "2. Kalkulus\n";
		cout << "Pilih : "; cin >> pilih;
		
		switch(pilih) {
			case 1:
				cout << "Nilai Harian\t: "; cin >> harian;
				cout << "Nilai UTS\t: "; cin >> uts;
				cout << "Nilai UAS\t: "; cin >> uas;
				nilai_akhir = (harian + uts + uas) / 3;
				if(nilai_akhir >= 80){
					cout << "\nAnda lulus Algoritma dan Pemrograman dengan nilai " << nilai_akhir << endl;
				} else {
					cout << "\nAnda tidak lulus Algoritma dan Pemrograman dengan nilai " << nilai_akhir << endl;
				}
			break;
			
			case 2:
				cout << "Nilai Harian\t: "; cin >> harian;
				cout << "Nilai UTS\t: "; cin >> uts;
				cout << "Nilai UAS\t: "; cin >> uas;
				nilai_akhir = (harian + uts + uas) / 3;
				if(nilai_akhir >= 80){
					cout << "\nAnda lulus Kalkulus dengan nilai " << nilai_akhir << endl;
				} else {
					cout << "\nAnda tidak lulus Kalkulus dengan nilai " << nilai_akhir << endl;
				}
			break;
			
			default : cout << "\nInput anda salah!\n";
		}
		
	} else if (username != "admin" && password == "123"){
		cout << "\nUsername anda salah!\n";
		
	} else if (username == "admin" && password != "123"){
		cout << "\nPassword anda salah!\n";
		
	} else {
		cout << "\nUsername dan Password anda salah!\n";
		
	}
	
	system ("pause");
	
	return 0;
}
