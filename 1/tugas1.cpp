#include <iostream>
#include <windows.h>
using namespace std;

int main(){
    string nama, nim;
    int uts, uas, akhir;
    
    cout << "Nama\t: "; cin >> nama;
    cout << "NIM\t: "; cin >> nim;
    cout << "UTS\t: "; cin >> uts;
    cout << "UAS\t: "; cin >> uas;
    
    akhir = (uts + uas) / 2;
    
    cout << endl;
    cout << "Hai, nama saya " << nama << ", nim " << nim << endl;
    cout << "Nilai akhir saya " << akhir << endl << endl;
    
    system("pause");
    return 0;
}

