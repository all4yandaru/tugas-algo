#include <bits/stdc++.h>
using namespace std;

void proseslogin();
void tambah();
void lihat();
void cari();
void ketemu(int index);

string mhs_nama[100], mhs_namamk[100][100];
int mhs_nim[100], mhs_mk[100], mhs_nilai[100], mhs_utsmk[100][100], mhs_uasmk[100][100], mhs_nilaimk[100][100];
bool login = false;
int jumlah = 0;

int main(){
	int pilih;
	proseslogin();
	
	system("pause");
	
	do
	{
		system("cls");
		cout<< "Menu" << endl;
		cout<< "1. Tambah Mahasiswa \n2. Lihat Mahasiswa \n3. Cari Mahasiswa \n4. Logout" << endl;
		cout<< "Pilih : "; cin >> pilih;
		switch (pilih)
		{
			case 1:
				tambah();
				break;
			case 2:
				lihat();
				break;
			case 3:
				cari();
				break;
			case 4:
				login = false;
				system("cls");
				proseslogin();
				break;
			default:
				cout<< "Salah Input" << endl;
		}
		cout<< endl << endl;
		system("pause");
	} while (true && login);
	
	return 0;
}

void proseslogin(){
	string user, pass;
	do
	{
		cout<< "Login Admin" << endl;
		cout<< "Username : "; cin >> user;
		cout<< "Password : "; cin >> pass;
		if (user == "admin" && pass == "admin123")
		{
			cout<< "Login sukses" << endl;
			login = true;
		}else if (user == "admin")
		{
			cout<< "Password salah" << endl;
		}else if (pass == "admin123")
		{
			cout<< "Username salah" << endl;
		}else {
			cout<< "Username dan Password salah" << endl;
		}
		cout<< endl;
	} while (!login);
	cout<< endl << endl;
}

void tambah(){
	int banyak;
	int temp;
	cout<< "Masukkan banyak mahasiswa : "; cin >> banyak;
	for (int i = jumlah; i < (jumlah + banyak); i++)
	{
		cout<< "Masukkan data mahasiswa ke-" << i+1 << endl;
		cout<< "Nama \t: "; cin.ignore(); getline(cin,mhs_nama[i]);
		cout<< "Nim \t: "; cin >> mhs_nim[i];
		cout<< "Banyak MK : "; cin >> mhs_mk[i];
		temp = 0;
		for (int j = 0; j < mhs_mk[i]; j++)
		{
			cout<< "\tMatkul " << j+1 << endl;
			cout<< "\tNama \t: "; cin.ignore(); getline(cin,mhs_namamk[i][j]);
			cout<< "\tUTS \t: "; cin >> mhs_utsmk[i][j];
			cout<< "\tUAS \t: "; cin >> mhs_uasmk[i][j];
			mhs_nilaimk[i][j] = (mhs_utsmk[i][j] + mhs_uasmk[i][j])/2;
			temp = temp + mhs_nilaimk[i][j];
		}
		mhs_nilai[i] = temp/mhs_mk[i];
		cout<< endl;
	}
	jumlah = jumlah + banyak;
	cout<< "Berhasil memasukkan data mahasiswa" << endl;
}

void lihat(){
	cout<< "Jumlah mahasiswa : " << jumlah << endl;
	for (int i = 0; i < jumlah; i++)
	{
		cout<< "Data mahasiswa ke-" << i+1 << endl;
		cout<< "Nama \t: " << mhs_nama[i] << endl;
		cout<< "Nim \t: " << mhs_nim[i] << endl;
		for (int j = 0; j < mhs_mk[i]; j++)
		{
			cout<< "\tMatkul " << j+1 << endl;
			cout<< "\tNama \t: " << mhs_namamk[i][j] << endl;
			cout<< "\tNilai \t: " << mhs_nilaimk[i][j] << endl;
		}
		cout<< "Nilai Akhir : " << mhs_nilai[i] << endl;
		cout<< endl;
	}
}

void cari(){
	string cari;
	int index;
	bool dapat = false;
	cout<< "Cari nama mahasiswa : "; cin.ignore(); getline(cin,cari);
	for (int i = 0; i < jumlah; i++)
	{
		if (cari == mhs_nama[i])
		{
			dapat = true;
			index = i;
			break;
		}
	}
	if (dapat)
	{
		ketemu(index);
	}
	else
		cout<< "Data tidak ditemukan" << endl;
}

void ketemu(int index){
	char update;
	int temp;
	
	cout<< "Data ditemukan" << endl;
	cout<< "Data mahasiswa ke-" << index+1 << endl;
	cout<< "Nama \t: " << mhs_nama[index] << endl;
	cout<< "Nim \t: " << mhs_nim[index] << endl;
	for (int j = 0; j < mhs_mk[index]; j++)
	{
		cout<< "\tMatkul " << j+1 << endl;
		cout<< "\tNama \t: " << mhs_namamk[index][j] << endl;
		cout<< "\tNilai \t: " << mhs_nilaimk[index][j] << endl;
	}
	cout<< "Nilai Akhir : " << mhs_nilai[index] << endl;
	cout<< endl;

	cout<< "(Bonus) Update data (y/n) ? "; cin >> update;
	if (update == 'Y' || update == 'y')
	{
		cout<< "Update data mahasiswa ke-" << index+1 << endl;
		cout<< "Nama \t: "; cin.ignore(); getline(cin,mhs_nama[index]);
		cout<< "Nim \t: " << mhs_nim[index] << endl;
		temp = 0;
		for (int j = 0; j < mhs_mk[index]; j++)
		{
			cout<< "\tMatkul " << j+1 << endl;
			cout<< "\tNama \t: "; getline(cin,mhs_namamk[index][j]);
			cout<< "\tUTS \t: "; cin >> mhs_utsmk[index][j];
			cout<< "\tUAS \t: "; cin >> mhs_uasmk[index][j];
			mhs_nilaimk[index][j] = (mhs_utsmk[index][j] + mhs_uasmk[index][j])/2;
			temp = temp + mhs_nilaimk[index][j];
			cin.ignore(); 
		}
		mhs_nilai[index] = temp/mhs_mk[index];
		cout<< endl;
		cout<< "Update berhasil" << endl;
	}
}

