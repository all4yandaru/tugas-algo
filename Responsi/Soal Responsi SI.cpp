#include <bits/stdc++.h>
using namespace std;

void proseslogin();
void tambah();
void lihat();
void cari();
void ketemu(int index);

string mkn_nama[100], mkn_namabh[100][100];
int mkn_bh[100], mkn_total[100], mkn_jumlah[100][100], mkn_hargabh[100][100], mkn_harga[100][100];
bool login = false;
int jumlah = 0;

int main(){
	int pilih;
	proseslogin();
	
	system("pause");
	
	do
	{
		system("cls");
		cout<< "Menu" << endl;
		cout<< "1. Tambah Makanan \n2. Lihat Makanan \n3. Cari Makanan \n4. Logout" << endl;
		cout<< "Pilih : "; cin >> pilih;
		switch (pilih)
		{
			case 1:
				tambah();
				break;
			case 2:
				lihat();
				break;
			case 3:
				cari();
				break;
			case 4:
				login = false;
				system("cls");
				proseslogin();
				break;
			default:
				cout<< "Salah Input" << endl;
		}
		cout<< endl << endl;
		system("pause");
	} while (true && login);
	
	return 0;
}

void proseslogin(){
	string user, pass;
	do
	{
		cout<< "Login Admin" << endl;
		cout<< "Username : "; cin >> user;
		cout<< "Password : "; cin >> pass;
		if (user == "admin" && pass == "admin123")
		{
			cout<< "Login sukses" << endl;
			login = true;
		}else if (user == "admin")
		{
			cout<< "Password salah" << endl;
		}else if (pass == "admin123")
		{
			cout<< "Username salah" << endl;
		}else {
			cout<< "Username dan Password salah" << endl;
		}
		cout<< endl;
	} while (!login);
	cout<< endl << endl;
}

void tambah(){
	int banyak;
	int temp;
	cout<< "Masukkan banyak makanan : "; cin >> banyak;
	for (int i = jumlah; i < (jumlah + banyak); i++)
	{
		cout<< "Masukkan data makanan ke-" << i+1 << endl;
		cout<< "Nama \t: "; cin.ignore(); getline(cin,mkn_nama[i]);
		cout<< "Banyak Bahan : "; cin >> mkn_bh[i];
		temp = 0;
		for (int j = 0; j < mkn_bh[i]; j++)
		{
			cout<< "\tBahan " << j+1 << endl;
			cout<< "\tNama \t: "; cin.ignore(); getline(cin,mkn_namabh[i][j]);
			cout<< "\tJumlah \t: "; cin >> mkn_jumlah[i][j];
			cout<< "\tHarga \t: "; cin >> mkn_hargabh[i][j];
			mkn_harga[i][j] = mkn_jumlah[i][j]*mkn_hargabh[i][j];
			temp = temp + mkn_harga[i][j];
		}
		mkn_total[i] = temp;
		cout<< endl;
	}
	jumlah = jumlah + banyak;
	cout<< "Berhasil memasukkan data makanan" << endl;
}

void lihat(){
	cout<< "Jumlah makanan : " << jumlah << endl;
	for (int i = 0; i < jumlah; i++)
	{
		cout<< "Data makanan ke-" << i+1 << endl;
		cout<< "Nama \t: " << mkn_nama[i] << endl;
		for (int j = 0; j < mkn_bh[i]; j++)
		{
			cout<< "\tBahan " << j+1 << endl;
			cout<< "\tNama \t: " << mkn_namabh[i][j] << endl;
			cout<< "\tHarga \t: " << mkn_harga[i][j] << endl;
		}
		cout<< "Total Harga : " << mkn_total[i] << endl;
		cout<< endl;
	}
}

void cari(){
	string cari;
	int index;
	bool dapat = false;
	cout<< "Cari nama makanan : "; cin.ignore(); getline(cin,cari);
	for (int i = 0; i < jumlah; i++)
	{
		if (cari == mkn_nama[i])
		{
			dapat = true;
			index = i;
			break;
		}
	}
	if (dapat)
	{
		ketemu(index);
	}
	else
		cout<< "Data tidak ditemukan" << endl;
}

void ketemu(int index){
	char update;
	int temp;
	
	cout<< "Data ditemukan" << endl;
	cout<< "Data makanan ke-" << index+1 << endl;
	cout<< "Nama \t: " << mkn_nama[index] << endl;
	for (int j = 0; j < mkn_bh[index]; j++)
	{
		cout<< "\tBahan " << j+1 << endl;
		cout<< "\tNama \t: " << mkn_namabh[index][j] << endl;
		cout<< "\tHarga \t: " << mkn_harga[index][j] << endl;
	}
	cout<< "Total Harga : " << mkn_total[index] << endl;
	cout<< endl;

	cout<< "(Bonus) Update data (y/n) ? "; cin >> update;
	if (update == 'Y' || update == 'y')
	{
		cout<< "Update data makanan ke-" << index+1 << endl;
		cout<< "Nama \t: "; cin.ignore(); getline(cin,mkn_nama[index]);
		temp = 0;
		for (int j = 0; j < mkn_bh[index]; j++)
		{
			cout<< "\tBahan " << j+1 << endl;
			cout<< "\tNama \t: "; getline(cin,mkn_namabh[index][j]);
			cout<< "\tJumlah \t: "; cin >> mkn_jumlah[index][j];
			cout<< "\tHarga \t: "; cin >> mkn_hargabh[index][j];
			mkn_harga[index][j] = mkn_jumlah[index][j]*mkn_hargabh[index][j];
			temp = temp + mkn_harga[index][j];
			cin.ignore(); 
		}
		mkn_total[index] = temp;
		cout<< endl;
		cout<< "Update berhasil" << endl;
	}
}