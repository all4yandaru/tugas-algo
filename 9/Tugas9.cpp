#include <iostream>
using namespace std;

int banyak;

int getMin(int[]);
int getMax(int[]);
double getMean(int[]);
int getSelisih(int, int);
int getSelisihTerbesar(int[]);

int main()
{
	cout << "Masukkan banyak angka : ";
	cin >> banyak;

	int angka[banyak];

	for (int i = 0; i < banyak; i++)
	{
		cout << "Angka ke-" << i + 1 << " : ";
		cin >> angka[i];
	}
	cout << endl;

	cout << "\n_____Hasil_____\n";
	cout << "Deret       : ";
	for (int i = 0; i < banyak; i++)
	{
		cout << angka[i] << " ";
	}
	cout << endl;
	cout << "Maksimum    : " << getMax(angka) << endl;
	cout << "Minimum     : " << getMin(angka) << endl;
	cout << "Rata-rata   : " << getMean(angka) << endl;
	cout << "Selisih Max : " << getSelisihTerbesar(angka) << endl;
	
	system("pause");
}

int getSelisihTerbesar(int angka[])
{
	int selisih = 0;
	for (int i = 0; i < banyak - 1; i++)
	{
		int tempSelisih = getSelisih(angka[i], angka[i + 1]);
		if (selisih < tempSelisih)
		{
			selisih = tempSelisih;
		}
	}
	return selisih;
}

int getSelisih(int a, int b)
{
	int selisih = (a > b) ? a - b : b - a;
	return selisih;
}

double getMean(int angka[])
{
	int total = 0;
	for (int i = 0; i < banyak; i++)
	{
		total += angka[i];
	}
	double mean = total * 1.0 / banyak;
	return mean;
}

int getMax(int angka[])
{
	int max = angka[0];
	for (int i = 0; i < banyak; i++)
	{
		if (max < angka[i])
		{
			max = angka[i];
		}
	}
	return max;
}

int getMin(int angka[])
{
	int min = angka[0];
	for (int i = 0; i < banyak; i++)
	{
		if (min > angka[i])
		{
			min = angka[i];
		}
	}
	return min;
}