#include <iostream>
#include <windows.h>
using namespace std;

/*
1. 40
2. 60
3. 20
*/

/*
123180053
int 123180053 / 1000000 = 123


*/

int main()
{
	int pilih, x, y, nim;
	float kuis, responsi, tugas, project, rata;
	string nama, jurusan;
	char nilai_huruf;

	cout << "KUISSS ================ \n";
	cout << "1. Deret Unik\n2. Nilai Akhir\n3. Gunting Batu Kertas (Bonus)\n4. Keluar\nPilih: ";
	cin >> pilih;

	switch (pilih)
	{
	case 1:
		cout << "X : ";
		cin >> x; // 4 // 8 div 4 = 2
		cout << "Y : ";
		cin >> y; // 15

		cout << endl;

		for (int i = 1; i <= y; i++) // -
		{
			if (i % x == 0)
			{
				cout << "|";
			}
			else
			{
				cout << "-";
			}
		}

		break;

	case 2:
		cout << "\nInput Nilai Praktikum =========\n";
		cout << "Nama		: ";
		cin >> nama;
		cout << "NIM		: ";
		cin >> nim;
		cout << "Kuis		: ";
		cin >> kuis;
		cout << "Responsi	: ";
		cin >> responsi;
		cout << "Tugas		: ";
		cin >> tugas;
		cout << "Project	\t: ";
		cin >> project;

		// kuis 20%, responsi 35%, tugas 15%, project 30%
		rata = (0.2 * kuis) + (0.35 * responsi) + (0.15 * tugas) + (0.3 * project);

		if (nim / 1000000 == 121)
		{
			jurusan = "Teknik Kimia";
		}
		else if (nim / 1000000 == 122)
		{
			jurusan = "Teknik Industri";
		}
		else if (nim / 1000000 == 123)
		{
			jurusan = "Informatika";
		}
		else if (nim / 1000000 == 124)
		{
			jurusan = "Sistem Informasi";
		}
		else
		{
			jurusan = "Salah Jurusan";
		}

		if (rata >= 80 && rata <= 100)
		{
			nilai_huruf = 'A';
		}
		else if (rata >= 60)
		{
			nilai_huruf = 'B';
		}
		else if (rata >= 40)
		{
			nilai_huruf = 'C';
		}
		else if (rata >= 20)
		{
			nilai_huruf = 'D';
		}
		else
		{
			nilai_huruf = 'E';
		}

		cout << endl;
		cout << "Hasil Akhir ===================\n";
		cout << "Nama\t\t: " << nama << endl;
		cout << "NIM\t\t: " << nim << endl;
		cout << "Jurusan\t\t: " << jurusan << endl;
		cout << "Rata-rata\t: " << rata << endl;
		cout << "Nilai Huruf\t: " << nilai_huruf << endl;
		if (rata >= 60)
		{
			cout << "Selamat anda lulus praktikum" << endl;
		}
		else
		{
			cout << "Mohon maaf anda belum lulus praktikum" << endl;
		}
		break;

	case 3:
	{
		string player1, player2, tangan1, tangan2;
		int menang1 = 0;
		int menang2 = 0;
		int ronde;

		cout << "Masukkan nama player 1 : ";
		cin >> player1;
		cout << "Masukkan nama player 2 : ";
		cin >> player2;

		cout << "Masukkan banyak ronde : ";
		cin >> ronde; // 4

		for (int i = 0; i < ronde; i++) // 0 1 2 3
		{
			cout << "\nRonde ke-" << i + 1 << endl;

			cout << "Masukkan kertas/gunting/batu " << player1 << " : ";
			cin >> tangan1;
			cout << "Masukkan kertas/gunting/batu " << player2 << " : ";
			cin >> tangan2;

			if (tangan1 == "kertas") // player1 = kertas --> player2 = batu
			{
				if (tangan2 == "batu")
					menang1++;
				else if (tangan2 == "gunting")
					menang2++;
			}
			else if (tangan1 == "gunting")
			{
				if (tangan2 == "kertas")
					menang1++;
				else if (tangan2 == "batu")
					menang2++;
			}
			else if (tangan1 == "batu")
			{
				if (tangan2 == "kertas")
					menang2++;
				else if (tangan2 == "gunting")
					menang1++;
			}
		}

		cout << endl
				 << "_____Hasil Akhir_____" << endl;
		cout << player1 << " menang " << menang1 << " kali" << endl;
		cout << player2 << " menang " << menang2 << " kali" << endl;
		if (menang1 > menang2)
			cout << "Jadi pemenangnya adalah " << player1 << endl;
		else if (menang2 > menang1)
			cout << "Jadi pemenangnya adalah " << player2 << endl;
		else
			cout << "Pertandingan seri" << endl;
	}
	break;

	case 4:
		exit(0);

	default:
		cout << "Mohon maaf anda salah input" << endl;
	}

	cout << endl;
	system("pause");
	return 0;
}
