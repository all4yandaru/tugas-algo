#include <iostream>
#include <windows.h>
using namespace std;

int main(){
	int pilih;
	int panjang, lebar, tinggi, temp;
	
	cout << "Gambar Bangun Datar ===========\n";
	cout << "1. Kotak bolong\n2. Segitiga\nPilih : "; cin >> pilih;
	
	cout << endl;
	
	switch(pilih){
		case 1:
			cout << "Panjang\t: "; cin >> panjang;
			cout << "Lebar\t: "; cin >> lebar;
			
			for (int i = 0; i < panjang; i++)
			{
				for (int j = 0; j < lebar; j++)
				{
					if (i == 0 || i == panjang-1 || j == 0 || j == lebar-1)
					{
						cout << "* ";
					}
					else {
						cout << "  ";
					}
				}
				cout << endl;
			}
			
		break;
		
		case 2:
			cout << "Tinggi\t: "; cin >> tinggi;
			for (int i = 0; i < tinggi; i++)
			{
				temp = i + 1;
				for (int j = 0; j < i+1; j++)
				{
					cout << temp << " ";
					temp = temp + tinggi-(j+1);
				}
				cout << endl;
			}
			
		break;
		
		default: cout << "Input anda salah!\n";
	}
	
	cout << endl;
	system("pause");
	return 0;
}
